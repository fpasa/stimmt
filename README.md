<div align="center">
    <img alt="stimmt logo" src="logo.png" width="320" />
</div>

# stimmt

An efficient tiled image format for very large imagery, complete with overviews 
for fast viewing, support for arbitrary channels and diverse data types,
and custom image metadata.

## Motivation

For certain applications, such as geographic data analysis and medical imaging,
it's necessary to work with image data of large size (think 100k*100k pixels),
often also in high bit num_channels, and custom number of channels.

For these images, it becomes fundamental to store tiles, and to pre-compute
downscaled versions for ease of viewing (similar to web maps, where tiles
at different scales are used). One other problem is that the sheer size of
the images make compression very important, and it's also sometimes important
to have lossless compression available, not to lose data.

stimmt is designed to solve these problems. In it's essence it's very similar to
TIFF files, but is simpler:

- stimmt can only store tiled images (which works well also for small images).
- stimmt does not offer so many options, but tries to be minimal and efficient.
  Writing a parser should be easy in any language.
- Offer a single, high quality compression option.

## Currently supported features

- Images with edge size up to about 4 billion pixels.
- Arbitrary channel number.
- For now, supports u8 data.
- Tiled images.
- Tags can contain arbitrary data, but a bunch of fields is standardized.
- Support for overviews (downscaled versions).
- Fast rust reference implementation.

## Format specification

[See here.](docs/specification.md)

## Status

The library is a work in progress, for now the basic infrastructure for the
reading and writing (uncompressed) of files is there.

What currently works is:

- headers (writing & parsing)
- tags (writing & parsing)
- uncompressed image data (writing & reading)

What's missing:

- overviews
- resampling support for overviews
- compression 

## Roadmap

- Complete reference implementation.
- Detailed format specification.
- Include tooling with crate:
  - CLI app to convert from and to basic image formats (JPG, PNG)
  - Tiled tiff conversion
  - Viewer
- Better test coverage.
- Design better API.

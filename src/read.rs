use super::*;

use std::io::Read;
use std::str::from_utf8;

pub(crate) fn read_string(reader: &mut impl Read, len: u32) -> Result<String, Error> {
    let mut buf = vec![0; len as usize];
    reader.read(buf.as_mut_slice())?;
    Ok(from_utf8(&buf)?.to_string())
}

pub(crate) fn read_u32(reader: &mut impl Read) -> Result<u32, Error> {
    let mut buf: [u8; 4] = [0; 4];
    reader.read(&mut buf)?;
    Ok(u32::from_le_bytes(buf))
}

pub(crate) fn read_u64(reader: &mut impl Read) -> Result<u64, Error> {
    let mut buf: [u8; 8] = [0; 8];
    reader.read(&mut buf)?;
    Ok(u64::from_le_bytes(buf))
}

pub(crate) fn read_i64(reader: &mut impl Read) -> Result<i64, Error> {
    let mut buf: [u8; 8] = [0; 8];
    reader.read(&mut buf)?;
    Ok(i64::from_le_bytes(buf))
}

pub(crate) fn read_i64_vec(reader: &mut impl Read, len: u32) -> Result<Vec<i64>, Error> {
    let mut vec = vec![];
    for _ in 0..len {
        vec.push(read_i64(reader)?);
    }
    Ok(vec)
}

pub(crate) fn read_f64(reader: &mut impl Read) -> Result<f64, Error> {
    let mut buf: [u8; 8] = [0; 8];
    reader.read(&mut buf)?;
    Ok(f64::from_le_bytes(buf))
}

pub(crate) fn read_f64_vec(reader: &mut impl Read, len: u32) -> Result<Vec<f64>, Error> {
    let mut vec = vec![];
    for _ in 0..len {
        vec.push(read_f64(reader)?);
    }
    Ok(vec)
}

pub(crate) fn read_key(reader: &mut impl Read) -> Result<String, Error> {
    let mut buf: [u8; 4] = [0; 4];
    reader.read(&mut buf)?;
    Ok(from_utf8(&buf)?.to_string())
}

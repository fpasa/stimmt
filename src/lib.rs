use std::cmp::min;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

mod error;
pub use error::{Error, Error::*};
mod temp;
use temp::*;
mod read;
use read::*;
mod write;
use write::*;
mod basic_types;
pub use basic_types::*;
mod buffer;
pub use buffer::{Buffer::*, *};
mod parts;
pub use parts::*;
mod cache;
use cache::*;

const TILE_SIZE: u32 = 512;

trait SeekRead: Seek + Read {}
impl<T: Seek + Read> SeekRead for T {}

#[derive(Debug, Clone)]
pub(crate) struct TileCoverage {
    pos: Pos,
    tile: Option<Tile>,
    // Which part of the tile is covered
    coverage: Bounds,
    // Which region of the bounds area is represented
    region: Bounds,
}

pub struct StimmtImage<'a> {
    mode: ImageMode,
    datatype: DataType,
    width: u32,
    height: u32,
    num_channels: u32,
    overviews: BTreeMap<u32, Overview>,
    tags: BTreeMap<String, Tag>,
    reader: Option<Box<dyn SeekRead + 'a>>,
    writer: Option<Box<dyn Write + 'a>>,
    cache: Option<Box<dyn Cache + 'a>>,
}

impl<'a> std::fmt::Debug for StimmtImage<'a> {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            fmt,
            "<StimmtImage datatype={} size={}x{}x{}>",
            self.datatype, self.width, self.height, self.num_channels
        )
    }
}

impl<'a> StimmtImage<'a> {
    pub fn open_path(path: &Path) -> Result<Self, Error> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        Self::open(reader)
    }

    pub fn open(mut reader: impl Seek + Read + 'a) -> Result<Self, Error> {
        StimmtImage::verify_fingerprint(&mut reader)?;

        let mut image = Self {
            mode: ImageMode::Read,
            datatype: DataType::from_code(read_u32(&mut reader)?)?,
            width: read_u32(&mut reader)?,
            height: read_u32(&mut reader)?,
            num_channels: read_u32(&mut reader)?,
            overviews: BTreeMap::new(),
            tags: BTreeMap::new(),
            reader: Some(Box::new(reader)),
            writer: None,
            cache: None,
        };

        let num_overviews = read_u32(image.reader())?;
        let num_tags = read_u32(image.reader())?;

        image.read_overview_header(num_overviews)?;
        image.read_tags(num_tags)?;

        Ok(image)
    }

    pub fn create_path(
        path: &Path,
        datatype: DataType,
        width: u32,
        height: u32,
        channels: u32,
    ) -> Result<Self, Error> {
        let file = File::create(path)?;
        let writer = BufWriter::new(file);
        Self::create(writer, datatype, width, height, channels)
    }

    pub fn create(
        writer: impl Write + 'a,
        datatype: DataType,
        width: u32,
        height: u32,
        channels: u32,
    ) -> Result<Self, Error> {
        let mut overviews = BTreeMap::new();
        overviews.insert(1, Overview::new(1));

        Ok(Self {
            mode: ImageMode::Write,
            datatype: datatype.clone(),
            width,
            height,
            num_channels: channels,
            overviews,
            tags: BTreeMap::new(),
            reader: None,
            writer: Some(Box::new(writer)),
            cache: Some(Box::new(FileCache::new(datatype, channels)?)),
        })
    }

    pub fn datatype(&self) -> DataType {
        self.datatype.clone()
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn bounds(&self) -> Bounds {
        bounds(0, 0, self.width, self.height)
    }

    pub fn num_channels(&self) -> u32 {
        self.num_channels
    }

    pub fn overviews(&self) -> Vec<u32> {
        self.overviews.values().map(|v| v.scale_factor).collect()
    }

    pub fn num_overviews(&self) -> u32 {
        self.overviews.len() as u32
    }

    pub fn add_overview(&mut self, scale_factor: u32) {
        self.overviews
            .insert(scale_factor, Overview::new(scale_factor));
    }

    pub fn add_overviews(&mut self, scale_factors: &[u32]) {
        scale_factors
            .iter()
            .for_each(|scale_factor| self.add_overview(*scale_factor));
    }

    pub fn tags(&self) -> Vec<Tag> {
        self.tags.values().map(|t| t.clone()).collect()
    }

    pub fn tag(&self, key: &str) -> Option<Tag> {
        match self.tags.get(key) {
            Some(tag) => Some(tag.clone()),
            None => None,
        }
    }

    pub fn num_tags(&self) -> u32 {
        self.tags.len() as u32
    }

    pub fn add_tag(&mut self, key: &str, value: TagData) -> Result<(), Error> {
        if key.as_bytes().len() != 4 {
            return Err(ValueError(format!(
                "Tag key must be exactly 4 bytes in len, got '{}' which is {} bytes long.",
                key,
                key.as_bytes().len()
            )));
        }

        self.tags.insert(
            key.to_string(),
            Tag {
                key: key.to_string(),
                value,
            },
        );

        Ok(())
    }

    fn bounds_tiles(
        &self,
        bounds: &Bounds,
        overview: u32,
    ) -> Result<Vec<TileCoverage>, Error> {
        if bounds.end_x() > self.width || bounds.end_y() > self.height {
            return Err(BoundsError(format!(
                "Bounds {} are not contained in image of size {}, {}.",
                bounds, self.width, self.height
            )));
        }

        let sx = bounds.x / TILE_SIZE;
        let sy = bounds.y / TILE_SIZE;
        let ex = (bounds.end_x() + TILE_SIZE - 1) / TILE_SIZE;
        let ey = (bounds.end_y() + TILE_SIZE - 1) / TILE_SIZE;

        let mut tiles = vec![];
        for y in sy..ey {
            for x in sx..ex {
                let pos = (x * TILE_SIZE, y * TILE_SIZE);

                let tile = self.overviews[&overview]
                    .tiles
                    .get(&pos)
                    .and_then(|t| Some(t.clone()));

                fn clip_0(num: i64) -> u32 {
                    if num < 0 {
                        0
                    } else {
                        num as u32
                    }
                }

                // offsets
                let sox = clip_0(bounds.x as i64 - pos.0 as i64);
                let soy = clip_0(bounds.y as i64 - pos.1 as i64);
                let eox = clip_0(bounds.end_x() as i64 - pos.0 as i64);
                let eoy = clip_0(bounds.end_y() as i64 - pos.1 as i64);

                let rw = if eox > TILE_SIZE { TILE_SIZE } else { eox } - sox;
                let rh = if eoy > TILE_SIZE { TILE_SIZE } else { eoy } - soy;

                tiles.push(TileCoverage {
                    pos,
                    tile,
                    coverage: Bounds {
                        x: sox,
                        y: soy,
                        w: rw,
                        h: rh,
                    },
                    region: Bounds {
                        x: (pos.0 + sox) - bounds.x,
                        y: (pos.1 + soy) - bounds.y,
                        w: rw,
                        h: rh,
                    },
                });
            }
        }

        Ok(tiles)
    }

    pub fn read_win(&mut self, bounds: &Bounds) -> Result<Buffer, Error> {
        // TODO: add check for mode
        // TODO: support for reading specific overview
        if bounds.end_x() > self.width || bounds.end_y() > self.height {
            return Err(BoundsError(format!(
                "Trying to read window {} outside the image.",
                bounds
            )));
        }
        if !self.overviews.get(&1).unwrap().loaded {
            self.read_overview(1)?;
        }

        let mut buffer =
            Buffer::from_zeros(self.datatype, bounds.w, bounds.h, self.num_channels);

        let datatype = self.datatype.clone();
        let num_channels = self.num_channels;

        let tile_covs = self.bounds_tiles(bounds, 1)?;
        for tile_cov in tile_covs {
            let region = tile_cov.region;

            let tile_buf = match tile_cov.tile {
                Some(tile) => tile.read(self.reader(), datatype, num_channels)?,
                None => {
                    Buffer::from_zeros(self.datatype, region.w, region.h, num_channels)
                }
            };

            buffer.set_region(&region, &tile_buf)?;
        }

        Ok(buffer)
    }

    pub fn read(&mut self) -> Result<Buffer, Error> {
        // TODO: add check for mode & exact image size
        self.read_win(&self.bounds())
    }

    // fn check_dim(&self, bounds: &Bounds, dim: &BufferDim) -> Result<(), Error> {
    //     if dim.num_channels != self.num_channels {
    //         return Err(BoundsError(format!(
    //             "Data has {} channels but image has {}",
    //             dim.num_channels, self.num_channels
    //         )));
    //     }
    //
    //     if bounds.w != dim.width || bounds.h != dim.height {
    //         return Err(BoundsError(format!(
    //             "Buffer of size {}, {} does not match bounds of size {}, {}",
    //             dim.width, dim.height, bounds.w, bounds.h,
    //         )));
    //     }
    //
    //     Ok(())
    // }

    pub fn write_win(&mut self, bounds: &Bounds, data: &Buffer) -> Result<(), Error> {
        // TODO: add check for mode
        // TODO: support for writing specific overview
        if bounds.end_x() > self.width || bounds.end_y() > self.height {
            return Err(BoundsError(format!(
                "Trying to write window {} outside the image.",
                bounds
            )));
        }

        if bounds.w != data.width() || bounds.h != data.height() {
            return Err(BoundsError(format!(
                "Trying to write buffer of size {}, {} into window {} with different size.",
                data.width(),
                data.height(),
                bounds
            )));
        }

        if data.num_channels() != self.num_channels {
            return Err(BoundsError(format!(
                "Trying to write buffer with {} channels into image with {} channels.",
                data.num_channels(),
                self.num_channels
            )));
        }

        let tile_covs = self.bounds_tiles(bounds, 1)?;
        for tile_cov in tile_covs {
            let mut tile = match tile_cov.tile {
                // Tile not created and written yet
                None => Tile {
                    pos: tile_cov.pos,
                    width: min(self.width - tile_cov.pos.0, TILE_SIZE),
                    height: min(self.height - tile_cov.pos.1, TILE_SIZE),
                    offset: 0,
                    size: 0,
                },
                Some(tile) => tile,
            };

            let mut tile_buf = self.cache().get_cached_tile(&tile)?;
            let d = &data.get_region(&tile_cov.region)?;
            tile_buf.set_region(&tile_cov.coverage, d)?;

            self.cache().cache_tile(&mut tile, &tile_buf)?;

            let overview = self.overviews.get_mut(&1).unwrap();
            overview.tiles.insert(tile.pos, tile);
        }

        self.cache().flush()?;

        Ok(())
    }

    pub fn write(&mut self, data: &Buffer) -> Result<(), Error> {
        // TODO: add check for mode & exact image size
        self.write_win(&self.bounds(), data)?;
        Ok(())
    }

    fn reader(&mut self) -> &mut Box<dyn SeekRead + 'a> {
        self.reader.as_mut().unwrap()
    }

    fn writer(&mut self) -> &mut Box<dyn Write + 'a> {
        self.writer.as_mut().unwrap()
    }

    fn cache(&mut self) -> &mut Box<dyn Cache + 'a> {
        self.cache.as_mut().unwrap()
    }

    fn verify_fingerprint(reader: &mut impl Read) -> Result<(), Error> {
        let fingerprint = read_key(reader)?;

        if fingerprint != "stim" {
            return Err(ParseError("Not a stimmt file.".to_string()));
        }

        Ok(())
    }

    fn read_overview_header(&mut self, num_overviews: u32) -> Result<(), Error> {
        for _ in 0..num_overviews {
            let overview = Overview::read_header(self.reader())?;
            self.overviews.insert(overview.scale_factor, overview);
        }
        Ok(())
    }

    fn read_tags(&mut self, num_tags: u32) -> Result<(), Error> {
        for _ in 0..num_tags {
            let tag = Tag::read(self.reader())?;
            self.tags.insert(tag.key.clone(), tag);
        }
        Ok(())
    }

    fn read_overview(&mut self, scale_factor: u32) -> Result<(), Error> {
        let mut overview = match self.overviews.get_mut(&scale_factor) {
            Some(overview) => overview,
            None => {
                return Err(ValueError(format!(
                    "Overview with scale factor {} does not exists.",
                    scale_factor
                )))
            }
        };

        overview.loaded = true;
        let overview_offset = overview.offset;

        let reader = self.reader();

        reader.seek(SeekFrom::Start(overview_offset))?;

        let num_tiles = read_u64(reader)?;
        self.read_tiles(scale_factor, num_tiles)?;

        Ok(())
    }

    fn read_tiles(&mut self, scale_factor: u32, num_tiles: u64) -> Result<(), Error> {
        let width = self.width;
        let height = self.height;
        let mut last_tile = Tile::default();
        for _ in 0..num_tiles {
            let tile = Tile::read_header(self.reader(), width, height)?;

            if last_tile.offset != 0 {
                last_tile.size = tile.offset - last_tile.offset;

                self.overviews
                    .get_mut(&scale_factor)
                    .unwrap()
                    .tiles
                    .insert(last_tile.pos, last_tile);
            }

            last_tile = tile;
        }

        last_tile.size = read_u64(self.reader())? - last_tile.offset;

        self.overviews
            .get_mut(&scale_factor)
            .unwrap()
            .tiles
            .insert(last_tile.pos, last_tile);

        Ok(())
    }

    fn end_of_file_header(&self) -> u64 {
        28
    }

    fn end_of_offset_header(&self) -> u64 {
        self.end_of_file_header() + 12 * self.overviews.len() as u64
    }

    fn end_of_tags(&self) -> u64 {
        self.end_of_offset_header() + self.tags.values().fold(0, |sum, t| sum + t.size())
    }

    pub fn close(&mut self) -> Result<(), Error> {
        if self.mode == ImageMode::Read {
            return Err(Error::ValueError(
                "Cannot close file opened in read mode.".to_string(),
            ));
        }

        self.write_file_header()?;
        self.write_overview_header()?;
        self.write_tags()?;
        self.write_overviews()?;

        self.writer().flush()?;
        self.cache().close()?;

        Ok(())
    }

    fn write_file_header(&mut self) -> Result<(), Error> {
        let datatype_code = self.datatype.to_code();
        let width = self.width;
        let height = self.height;
        let num_channels = self.num_channels;
        let overview_len = self.overviews.len() as u32;
        let tags_len = self.tags.len() as u32;

        let writer = self.writer();

        writer.write(b"stim")?;

        write_u32(writer, datatype_code)?;

        write_u32(writer, width)?;
        write_u32(writer, height)?;
        write_u32(writer, num_channels)?;
        write_u32(writer, overview_len)?;
        write_u32(writer, tags_len)?;

        Ok(())
    }

    fn write_overview_header(&mut self) -> Result<(), Error> {
        let mut offset = self.end_of_tags();
        let overviews: Vec<_> =
            self.overviews.values().rev().map(|v| v.clone()).collect();

        let writer = self.writer();

        for overview in overviews.iter() {
            write_u32(writer, overview.scale_factor)?;
            write_u64(writer, offset)?;
            offset += overview.size();
        }

        Ok(())
    }

    fn write_tags(&mut self) -> Result<(), Error> {
        let tags: Vec<Tag> = self.tags.values().map(|v| v.clone()).collect();
        for tag in tags {
            tag.write(self.writer())?;
        }
        Ok(())
    }

    fn write_overviews(&mut self) -> Result<(), Error> {
        let mut overviews: Vec<Overview> =
            self.overviews.values().rev().map(|v| v.clone()).collect();

        let mut offset = self.end_of_tags();
        for overview in overviews.iter_mut() {
            self.write_overview(overview, offset)?;
            offset += overview.size();
        }

        Ok(())
    }

    fn write_overview(
        &mut self,
        overview: &mut Overview,
        offset: u64,
    ) -> Result<(), Error> {
        self.write_tile_header(overview, offset)?;

        for tile in overview.tiles.values_mut() {
            let buf = self.cache().get_cached_tile(&tile)?;
            tile.write(self.writer(), &buf)?;
        }

        Ok(())
    }

    pub(crate) fn write_tile_header(
        &mut self,
        overview: &mut Overview,
        offset: u64,
    ) -> Result<(), Error> {
        let writer = self.writer();

        let num_tiles = overview.tiles.len() as u64;

        write_u64(writer, num_tiles)?;

        let mut tile_offset = offset + overview.tile_header_size();
        for tile in overview.tiles.values() {
            write_u32(writer, tile.pos.0)?;
            write_u32(writer, tile.pos.1)?;
            write_u64(writer, tile_offset)?;
            tile_offset += tile.size;
        }
        write_u64(writer, tile_offset)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests;

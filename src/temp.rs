use std::path::PathBuf;

use rand::Rng;

pub(crate) fn temp_file(namespace: &str, filename: &str) -> PathBuf {
    let mut rng = rand::thread_rng();
    let random: u32 = rng.gen();

    let mut tmp_file = std::env::temp_dir();

    tmp_file.push(namespace.to_string());
    tmp_file.push(random.to_string().as_str());
    std::fs::create_dir_all(&tmp_file).unwrap();

    tmp_file.push(filename);
    tmp_file.clone()
}

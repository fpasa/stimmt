use super::*;

use std::cmp::{max, min};
use std::fmt::Display;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DataType {
    U8,
}

impl DataType {
    pub(crate) fn from_code(code: u32) -> Result<DataType, Error> {
        match code {
            0 => Ok(DataType::U8),
            _ => Err(ParseError(format!(
                "Invalid datatype: {} does not correspond to any supported datatype.",
                code
            ))),
        }
    }

    pub(crate) fn to_code(&self) -> u32 {
        match self {
            DataType::U8 => 0,
        }
    }
}

impl Display for DataType {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            DataType::U8 => write!(fmt, "u8"),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TagData {
    String(String),
    Int(i64),
    IntArray(Vec<i64>),
    Float(f64),
    FloatArray(Vec<f64>),
}

impl Eq for TagData {}

pub type Pos = (u32, u32);

#[derive(Debug, Clone)]
pub struct Bounds {
    pub x: u32,
    pub y: u32,
    pub w: u32,
    pub h: u32,
}

impl Bounds {
    pub fn end_x(&self) -> u32 {
        self.x + self.w
    }

    pub fn end_y(&self) -> u32 {
        self.y + self.h
    }

    pub fn intersect(&self, other: &Bounds) -> Bounds {
        let x = max(self.x, other.x);
        let y = max(self.y, other.y);
        Bounds {
            x,
            y,
            w: min(self.end_x(), other.end_x()) - x,
            h: min(self.end_y(), other.end_y()) - y,
        }
    }
}

impl Display for Bounds {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            fmt,
            "<{}, {} -> {}, {}>",
            self.x,
            self.y,
            self.end_x(),
            self.end_y()
        )
    }
}

pub fn bounds(x: u32, y: u32, w: u32, h: u32) -> Bounds {
    Bounds { x, y, w, h }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ImageMode {
    Read,
    Write,
}

use super::*;

use std::io::Cursor;

#[test]
fn write_header() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    let data = cursor.get_ref();

    assert_eq!(
        &data[..28],
        vec![
            // stim
            115, 116, 105, 109, //
            // datatype
            0, 0, 0, 0, //
            // width, height, channels
            128, 2, 0, 0, //
            224, 1, 0, 0, //
            3, 0, 0, 0, //
            // num overviews
            4, 0, 0, 0, //
            // num tags
            5, 0, 0, 0 //
        ]
        .as_slice()
    );
}

#[test]
fn write_overview_header() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    let data = cursor.get_ref();

    assert_eq!(
        &data[28..(28 + 12 * 4)],
        vec![
            // 8
            8, 0, 0, 0, //
            196, 0, 0, 0, 0, 0, 0, 0, //
            // 4
            4, 0, 0, 0, //
            212, 0, 0, 0, 0, 0, 0, 0, //
            // 2
            2, 0, 0, 0, //
            228, 0, 0, 0, 0, 0, 0, 0, //
            // 1
            1, 0, 0, 0, //
            244, 0, 0, 0, 0, 0, 0, 0, //
        ]
        .as_slice()
    );
}

#[test]
fn write_tags() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    let data = cursor.get_ref();

    assert_eq!(
        &data[(28 + 12 * 4)..(28 + 12 * 4 + 16)],
        vec![
            // key "aper"
            97, 112, 101, 114, //
            // datatype
            3, 0, 0, 0, //
            // data (float 1.7)
            51, 51, 51, 51, 51, 51, 251, 63
        ]
        .as_slice()
    );
    assert_eq!(
        &data[(28 + 12 * 4 + 16)..(28 + 12 * 4 + 40)],
        vec![
            // key "auth"
            97, 117, 116, 104, //
            // datatype
            0, 0, 0, 0, //
            // length
            9, 0, 0, 0, //
            // data (string "Francesco")
            70, 114, 97, 110, 99, 101, 115, 99, 111, 0, 0, 0 //
        ]
        .as_slice()
    );
    assert_eq!(
        &data[(28 + 12 * 4 + 40)..(28 + 12 * 4 + 76)],
        vec![
            // key "maxv"
            109, 97, 120, 118, //
            // datatype
            2, 0, 0, 0, //
            // length
            3, 0, 0, 0, //
            // data
            150, 0, 0, 0, 0, 0, 0, 0, //
            150, 0, 0, 0, 0, 0, 0, 0, //
            150, 0, 0, 0, 0, 0, 0, 0, //
        ]
        .as_slice()
    );
    assert_eq!(
        &data[(28 + 12 * 4 + 76)..(28 + 12 * 4 + 104)],
        vec![
            // key "refs"
            114, 101, 102, 115, //
            // datatype
            4, 0, 0, 0, //
            // length
            2, 0, 0, 0, //
            // data
            0, 0, 0, 0, 0, 0, 240, 63, //
            0, 0, 0, 0, 0, 0, 240, 63, //
        ]
        .as_slice()
    );
    assert_eq!(
        &data[(28 + 12 * 4 + 104)..(28 + 12 * 4 + 120)],
        vec![
            // key "year"
            121, 101, 97, 114, //
            // datatype
            1, 0, 0, 0, //
            // data
            228, 7, 0, 0, 0, 0, 0, 0, //
        ]
        .as_slice()
    );
}

#[test]
fn write_tile_header() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    let data = cursor.get_ref();

    assert_eq!(
        &data[244..(244 + 32)],
        vec![
            // num tiles
            1, 0, 0, 0, 0, 0, 0, 0, //
            // tile x and y
            0, 0, 0, 0, //
            0, 0, 0, 0, //
            // tile offset
            20, 1, 0, 0, 0, 0, 0, 0, //
            // final offset 276 + 737280
            20, 65, 11, 0, 0, 0, 0, 0, //
        ]
        .as_slice()
    );
}

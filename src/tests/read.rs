use super::*;

#[test]
fn read_header() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    cursor.seek(SeekFrom::Start(0)).unwrap();
    let image = StimmtImage::open(cursor).unwrap();

    assert_eq!(image.datatype(), DataType::U8);
    assert_eq!(image.width(), 640);
    assert_eq!(image.height(), 480);
    assert_eq!(image.num_channels(), 3);
    assert_eq!(image.num_overviews(), 4);
    assert_eq!(image.num_tags(), 5);
}

#[test]
fn read_overview_header() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    cursor.seek(SeekFrom::Start(0)).unwrap();
    let image = StimmtImage::open(cursor).unwrap();

    assert_eq!(image.overviews(), vec![1, 2, 4, 8]);
}

#[test]
fn read_tags() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    cursor.seek(SeekFrom::Start(0)).unwrap();
    let image = StimmtImage::open(cursor).unwrap();

    assert_eq!(
        image.tags(),
        vec![
            Tag {
                key: "aper".to_string(),
                value: TagData::Float(1.7),
            },
            Tag {
                key: "auth".to_string(),
                value: TagData::String("Francesco".to_string()),
            },
            Tag {
                key: "maxv".to_string(),
                value: TagData::IntArray(vec![150, 150, 150]),
            },
            Tag {
                key: "refs".to_string(),
                value: TagData::FloatArray(vec![1., 1.]),
            },
            Tag {
                key: "year".to_string(),
                value: TagData::Int(2020),
            },
        ]
    );
}

#[test]
fn read_tiles() {
    let mut cursor = Cursor::new(Vec::new());
    write_complete_image(&mut cursor);
    cursor.seek(SeekFrom::Start(0)).unwrap();
    let mut image = StimmtImage::open(cursor).unwrap();

    assert_eq!(image.overviews[&1].scale_factor, 1);
    assert_eq!(image.overviews[&1].offset, 244);
    assert_eq!(image.overviews[&1].loaded, false);
    assert_eq!(image.overviews[&1].tiles.len(), 0);

    image.read_overview(1).unwrap();

    assert_eq!(image.overviews[&1].scale_factor, 1);
    assert_eq!(image.overviews[&1].offset, 244);
    assert_eq!(image.overviews[&1].loaded, true);
    assert_eq!(image.overviews[&1].tiles.len(), 1);

    assert_eq!(image.overviews[&1].tiles[&(0, 0)].width, 512);
    assert_eq!(image.overviews[&1].tiles[&(0, 0)].height, 480);
    assert_eq!(image.overviews[&1].tiles[&(0, 0)].offset, 276);
    assert_eq!(image.overviews[&1].tiles[&(0, 0)].size, 737280);
}

use super::*;

mod buffer;
mod read;
mod write;

use std::path::PathBuf;
use std::time::Instant;

use crate::error::Error::IOError;
use std::io::Cursor;

fn temp_file_with_name(filename: &str) -> PathBuf {
    temp_file("stimmt_tests", filename)
}

fn write_complete_image(writer: impl Write) {
    let mut image = StimmtImage::create(writer, DataType::U8, 640, 480, 3).unwrap();

    image
        .add_tag("auth", TagData::String("Francesco".to_string()))
        .unwrap();
    image.add_tag("aper", TagData::Float(1.7)).unwrap();
    image.add_tag("year", TagData::Int(2020)).unwrap();
    image
        .add_tag("maxv", TagData::IntArray(vec![150, 150, 150]))
        .unwrap();
    image
        .add_tag("refs", TagData::FloatArray(vec![1., 1.]))
        .unwrap();

    image.add_overviews(&[2, 4, 8]);

    let buf = Buffer::from_value_u8(200, 50, 3, &[150, 150, 150]).unwrap();
    image.write_win(&bounds(50, 50, 200, 50), &buf).unwrap();

    image.close().unwrap();
}

// #[test]
// fn save_complete_image() {
//     let mut buf = BufWriter::new(File::create("image.stimmt").unwrap());
//     write_complete_image(buf);
// }

#[test]
fn test_load_invalid_file() {
    match StimmtImage::open_path("does_not_exist".as_ref()) {
        Err(IOError(_)) => {}
        _ => panic!("loading non-existing file does not cause the correct error"),
    }

    let mut filepath = std::env::current_dir().unwrap();
    filepath.push("Cargo.toml");
    match StimmtImage::open_path(&filepath) {
        Err(ParseError(_)) => {}
        _ => panic!("loading other file types does not cause the correct error"),
    }
}

#[test]
fn test_add_tag_wrong_key() {
    let tmp_file = temp_file_with_name("image.stimmt");

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 100, 100, 3).unwrap();
    let res = image.add_tag("author", TagData::String("Francesco".to_string()));
    match res {
        Err(_) => {}
        _ => panic!(
            "creating tags with a wrong key length does not cause an error: {:?}",
            res
        ),
    }
}

#[test]
fn file_header_loop() {
    // Test whether saving and loading header returns the same values
    let tmp_file = temp_file_with_name("image.stimmt");

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 100, 100, 3).unwrap();
    image.close().unwrap();

    let loaded_image = StimmtImage::open_path(&tmp_file).unwrap();

    assert_eq!(loaded_image.datatype(), image.datatype());
    assert_eq!(loaded_image.width(), image.width());
    assert_eq!(loaded_image.height(), image.height());
    assert_eq!(loaded_image.num_channels(), image.num_channels());
    assert_eq!(loaded_image.num_overviews(), image.num_overviews());
    assert_eq!(loaded_image.num_tags(), image.num_tags());
}

#[test]
fn overview_header_loop() {
    // Test whether saving and loading the overview header returns the same values
    let tmp_file = temp_file_with_name("image.stimmt");

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 100, 100, 3).unwrap();
    image.add_overviews(&[2, 4, 8]);
    image.close().unwrap();

    let loaded_image = StimmtImage::open_path(&tmp_file).unwrap();

    assert_eq!(loaded_image.num_overviews(), 4);
    assert_eq!(&loaded_image.overviews(), &[1, 2, 4, 8]);
}

#[test]
fn tags_loop() {
    // Test whether saving and loading tags returns the same values
    let tmp_file = temp_file_with_name("image.stimmt");

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 100, 100, 3).unwrap();
    image
        .add_tag("auth", TagData::String("Francesco".to_string()))
        .unwrap();
    image.add_tag("year", TagData::Int(2020)).unwrap();
    image
        .add_tag("yers", TagData::IntArray(vec![1, 2, 3]))
        .unwrap();
    image.add_tag("prec", TagData::Float(1.234)).unwrap();
    image
        .add_tag("pres", TagData::FloatArray(vec![1.2, 2.3, 3.4]))
        .unwrap();
    image.close().unwrap();

    let loaded_image = StimmtImage::open_path(&tmp_file).unwrap();

    assert_eq!(loaded_image.num_tags(), 5);
    assert_eq!(
        loaded_image.tag("auth").unwrap().value,
        TagData::String("Francesco".to_string())
    );
    assert_eq!(loaded_image.tag("year").unwrap().value, TagData::Int(2020));
    assert_eq!(
        loaded_image.tag("yers").unwrap().value,
        TagData::IntArray(vec![1, 2, 3])
    );
    assert_eq!(
        loaded_image.tag("prec").unwrap().value,
        TagData::Float(1.234)
    );
    assert_eq!(
        loaded_image.tag("pres").unwrap().value,
        TagData::FloatArray(vec![1.2, 2.3, 3.4])
    );
}

#[test]
fn write_image_data() {
    // Test whether saving and loading tags returns the same values
    let tmp_file = temp_file_with_name("image.stimmt");

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 100, 100, 3).unwrap();

    let buf = Buffer::from_value_u8(100, 100, 3, &[123, 123, 123]).unwrap();
    image.write(&buf).unwrap();

    image.close().unwrap();

    let mut loaded_image = StimmtImage::open_path(&tmp_file).unwrap();

    assert_eq!(loaded_image.read_win(&bounds(0, 0, 100, 100)).unwrap(), buf);
}

#[test]
fn write_image_data_buf_size_error() {
    // Test whether saving and loading tags returns the same values
    let tmp_file = temp_file_with_name("image.stimmt");

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 100, 100, 3).unwrap();

    let buf = Buffer::from_value_u8(200, 50, 3, &[123, 123, 123]).unwrap();
    let res = image.write(&buf);

    match res {
        Err(BoundsError(_)) => {}
        _ => panic!(
            "writing buffer of size different that the bounds \
            does not cause the correct error: {:?}",
            res
        ),
    }

    let buf = Buffer::from_value_u8(100, 100, 2, &[123, 123]).unwrap();
    let res = image.write(&buf);

    match res {
        Err(BoundsError(_)) => {}
        _ => panic!(
            "writing buffer of size different that the bounds \
            does not cause the correct error: {:?}",
            res
        ),
    }
}

#[test]
fn encoding_speed() {
    // Measure encoding speed for 10 MPixel image
    let tmp_file = temp_file_with_name("large.stimmt");

    let now = Instant::now();

    let mut image =
        StimmtImage::create_path(&tmp_file, DataType::U8, 4000, 2500, 3).unwrap();

    let buf = Buffer::from_value_u8(4000, 2500, 3, &[12, 134, 245]).unwrap();
    image.write(&buf).unwrap();
    image.close().unwrap();

    let elapsed = now.elapsed().as_secs_f32();
    println!("Elapsed: {:.2}", elapsed);

    assert!(elapsed < 100.);
}

#[test]
fn decoding_speed() {
    // Measure decoding speed for 10 MPixel image
    let now = Instant::now();

    let mut image = StimmtImage::open_path("test_data/large.stimmt".as_ref()).unwrap();
    image.read().unwrap();

    let elapsed = now.elapsed().as_secs_f32();
    println!("Elapsed: {:.2}", elapsed);

    assert!(elapsed < 40.);
}

#[test]
fn encoding_speed_memory() {
    // Measure encoding speed for 10 MPixel image
    let cursor = Cursor::new(Vec::new());

    let now = Instant::now();

    let mut image = StimmtImage::create(cursor, DataType::U8, 4000, 2500, 3).unwrap();

    let buf = Buffer::from_value_u8(4000, 2500, 3, &[12, 134, 245]).unwrap();
    image.write(&buf).unwrap();
    image.close().unwrap();

    let elapsed = now.elapsed().as_secs_f32();
    println!("Elapsed: {:.2}", elapsed);

    assert!(elapsed < 100.);
}

#[test]
fn decoding_speed_memory() {
    // Measure decoding speed for 10 MPixel image
    let mut file = File::open("test_data/large.stimmt").unwrap();
    let mut data = Vec::new();
    file.read_to_end(&mut data).unwrap();
    let cursor = Cursor::new(data);

    let now = Instant::now();

    let mut image = StimmtImage::open(cursor).unwrap();
    image.read().unwrap();

    let elapsed = now.elapsed().as_secs_f32();
    println!("Elapsed: {:.2}", elapsed);

    assert!(elapsed < 40.);
}

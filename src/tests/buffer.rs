use super::*;

fn test_buffer() -> Result<Buffer, Error> {
    let test_data = vec![
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
        23, 24,
    ];
    Buffer::from_data_u8(2, 4, 3, test_data.as_slice())
}

#[test]
fn buffer_from_zeros() {
    let res = Buffer::from_zeros(DataType::U8, 200, 50, 3);

    match res {
        Buffer::U8Buffer(data) => {
            assert_eq!(data.len(), 30_000);
            assert_eq!(data.values(), &vec![0; 30_000]);
        }
    }
}

#[test]
fn buffer_from_value_u8() {
    let buf = Buffer::from_value_u8(200, 50, 3, &[1, 2, 3]).unwrap();

    match buf {
        Buffer::U8Buffer(data) => {
            assert_eq!(data.len(), 30_000);
            assert_eq!(&data.values()[..6], &[1u8, 2, 3, 1, 2, 3]);
        }
    }
}

#[test]
fn buffer_from_value_u8_error() {
    let res = Buffer::from_value_u8(200, 50, 3, &[1, 2]);

    match res {
        Err(ValueError(_)) => {}
        _ => panic!(
            "from_value_u8 should fail if value is not as long as the number of channels"
        ),
    }
}

#[test]
fn buffer_from_data_u8() {
    let test_data = vec![
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
        23, 24,
    ];
    let buf = Buffer::from_data_u8(2, 4, 3, test_data.as_slice()).unwrap();

    match buf {
        Buffer::U8Buffer(data) => {
            assert_eq!(data.len(), 24);
            assert_eq!(data.values(), &test_data);
        }
    }
}

#[test]
fn buffer_from_data_u8_error() {
    let res = Buffer::from_data_u8(200, 50, 3, &[1, 2]);

    match res {
        Err(ValueError(_)) => {}
        _ => panic!(
            "from_data_u8 should fail if value is not as long as the total size of the buffer"
        ),
    }
}

#[test]
fn buffer_get_region() {
    let buf = test_buffer().unwrap();
    let region = buf.get_region(&bounds(1, 2, 1, 2)).unwrap();

    match region {
        Buffer::U8Buffer(data) => {
            assert_eq!(data.len(), 6);
            assert_eq!(data.values().as_slice(), &[16, 17, 18, 22, 23, 24]);
        }
    }
}

#[test]
fn buffer_get_region_errors() {
    let buf = test_buffer().unwrap();
    let res = buf.get_region(&bounds(1, 2, 2, 2));

    match res {
        Err(BoundsError(_)) => {}
        _ => panic!("get_region should fail when requesting data out of bounds"),
    }
}

#[test]
fn buffer_set_region() {
    let region = &bounds(1, 2, 1, 2);

    let mut buf = test_buffer().unwrap();
    buf.set_region(region, &Buffer::from_zeros(DataType::U8, 1, 2, 3))
        .unwrap();

    match buf {
        Buffer::U8Buffer(ref data) => {
            assert_eq!(data.len(), 24);
            assert_eq!(&data.values()[..6], &[1, 2, 3, 4, 5, 6]);
            match buf.get_region(region).unwrap() {
                Buffer::U8Buffer(region_data) => {
                    assert_eq!(region_data.values(), &[0, 0, 0, 0, 0, 0])
                }
            }
        }
    }
}

#[test]
fn buffer_set_region_errors() {
    let buf = test_buffer().unwrap();
    let res = buf.get_region(&bounds(1, 2, 2, 2));

    match res {
        Err(BoundsError(_)) => {}
        _ => panic!("get_region should fail when requesting data out of bounds"),
    }
}

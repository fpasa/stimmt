use std::io::Error as IOError;
use std::str::Utf8Error;

#[derive(Debug)]
pub enum Error {
    IOError(IOError),
    Utf8Error(Utf8Error),
    ParseError(String),
    BoundsError(String),
    ValueError(String),
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        return Error::IOError(err);
    }
}

impl From<Utf8Error> for Error {
    fn from(err: Utf8Error) -> Error {
        return Error::Utf8Error(err);
    }
}

use super::*;

use std::collections::BTreeMap;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Tag {
    pub key: String,
    pub value: TagData,
}

impl Tag {
    pub(crate) fn read(reader: &mut (impl Read + Seek)) -> Result<Tag, Error> {
        Ok(Tag {
            key: read_key(reader)?,
            value: match read_u32(reader)? {
                0 => {
                    let len = read_u32(reader)?;
                    let value = TagData::String(read_string(reader, len)?);
                    // skip padding
                    reader.seek(SeekFrom::Current(4 - len as i64 % 4))?;
                    value
                }
                1 => TagData::Int(read_i64(reader)?),
                2 => {
                    let len = read_u32(reader)?;
                    TagData::IntArray(read_i64_vec(reader, len)?)
                }
                3 => TagData::Float(read_f64(reader)?),
                4 => {
                    let len = read_u32(reader)?;
                    TagData::FloatArray(read_f64_vec(reader, len)?)
                }
                other => {
                    return Err(ParseError(format!(
                        "Unrecognized tag datatype {}.",
                        other
                    )))
                }
            },
        })
    }

    pub(crate) fn write(&self, writer: &mut impl Write) -> Result<(), Error> {
        writer.write(self.key.as_bytes())?;
        write_u32(
            writer,
            match self.value {
                TagData::String(_) => 0,
                TagData::Int(_) => 1,
                TagData::IntArray(_) => 2,
                TagData::Float(_) => 3,
                TagData::FloatArray(_) => 4,
            },
        )?;

        if self.list_datatype() {
            write_u32(writer, self.len()? as u32)?;
        }

        match self.value {
            TagData::String(ref value) => {
                writer.write(value.as_bytes())?;
                for _ in 0..(4 - value.len() % 4) {
                    writer.write(&[0u8])?;
                }
            }
            TagData::Int(ref value) => write_i64(writer, *value)?,
            TagData::IntArray(ref value) => {
                for v in value.iter() {
                    write_i64(writer, *v)?;
                }
            }
            TagData::Float(ref value) => write_f64(writer, *value)?,
            TagData::FloatArray(ref value) => {
                for v in value.iter() {
                    write_f64(writer, *v)?;
                }
            }
        }

        Ok(())
    }

    pub fn list_datatype(&self) -> bool {
        match self.value {
            TagData::Int(_) | TagData::Float(_) => false,
            _ => true,
        }
    }

    pub fn len(&self) -> Result<usize, Error> {
        match self.value {
            TagData::Int(_) | TagData::Float(_) => Err(ValueError(
                ".len() cannot be used on single value tags.".to_string(),
            )),
            TagData::String(ref val) => Ok(val.len()),
            TagData::IntArray(ref val) => Ok(val.len()),
            TagData::FloatArray(ref val) => Ok(val.len()),
        }
    }

    pub fn size(&self) -> u64 {
        if self.list_datatype() {
            12 + self.len().unwrap() as u64
                * match self.value {
                    TagData::Int(_) | TagData::Float(_) => {
                        panic!("Trying to get len of element of scalar.")
                    }
                    TagData::String(_) => 1,
                    TagData::IntArray(_) => 8,
                    TagData::FloatArray(_) => 8,
                }
                // strings might have padding
                + if let TagData::String(ref val) = self.value {
                    4 - val.len() as u64 % 4
                } else {
                    0
                }
        } else {
            16
        }
    }
}

#[derive(Debug, Clone, Default)]
pub(crate) struct Tile {
    pub(crate) pos: Pos,
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) offset: u64,
    pub(crate) size: u64,
}

impl Tile {
    pub(crate) fn read_header(
        reader: &mut impl Read,
        image_width: u32,
        image_height: u32,
    ) -> Result<Tile, Error> {
        let pos = (read_u32(reader)?, read_u32(reader)?);

        fn clip_tile_size(num: u32) -> u32 {
            if num > TILE_SIZE {
                TILE_SIZE
            } else {
                num
            }
        }

        Ok(Tile {
            pos,
            width: clip_tile_size(image_width - pos.0),
            height: clip_tile_size(image_height - pos.1),
            offset: read_u64(reader)?,
            size: 0,
        })
    }

    pub(crate) fn read(
        &self,
        reader: &mut (impl Seek + Read),
        datatype: DataType,
        num_channels: u32,
    ) -> Result<Buffer, Error> {
        reader.seek(SeekFrom::Start(self.offset))?;

        let mut buf = vec![0; self.size as usize];
        reader.read(buf.as_mut_slice())?;

        match datatype {
            DataType::U8 => {
                Buffer::from_data_u8(self.width, self.height, num_channels, &buf)
            }
        }
    }

    pub(crate) fn write(
        &mut self,
        writer: &mut impl Write,
        data: &Buffer,
    ) -> Result<u64, Error> {
        if data.width() != self.width && data.height() != self.height {
            return Err(ValueError(format!(
                "Trying to write buffer of size {}, {}, to tile of size {}, {}.",
                data.width(),
                data.height(),
                self.width,
                self.height,
            )));
        }

        // let mut cursor = Cursor::new();

        match data {
            U8Buffer(buf) => {
                self.size = writer.write(buf.values())? as u64;
                Ok(self.size)
            }
        }

        // self.size =
    }
}

#[derive(Debug, Clone)]
pub(crate) struct Overview {
    pub(crate) scale_factor: u32,
    pub(crate) offset: u64,
    pub(crate) loaded: bool,
    pub(crate) tiles: BTreeMap<Pos, Tile>,
}

impl Overview {
    pub(crate) fn new(scale_factor: u32) -> Self {
        Self {
            scale_factor,
            offset: 0,
            loaded: false,
            tiles: BTreeMap::new(),
        }
    }

    pub(crate) fn read_header(reader: &mut impl Read) -> Result<Overview, Error> {
        Ok(Overview {
            scale_factor: read_u32(reader)?,
            offset: read_u64(reader)?,
            loaded: false,
            tiles: BTreeMap::new(),
        })
    }

    pub(crate) fn tile_header_size(&self) -> u64 {
        // number of tiles
        8 +
        // tile header
        self.tiles.len() as u64 * 16 +
        // final offset
        8
    }

    pub(crate) fn size(&self) -> u64 {
        self.tile_header_size() + self.tiles.values().fold(0, |sum, t| sum + t.size)
    }
}

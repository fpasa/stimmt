use super::*;

use std::io::Write;

pub(crate) fn write_u32(writer: &mut impl Write, num: u32) -> Result<(), Error> {
    writer.write(&num.to_le_bytes())?;
    Ok(())
}

pub(crate) fn write_u64(writer: &mut impl Write, num: u64) -> Result<(), Error> {
    writer.write(&num.to_le_bytes())?;
    Ok(())
}

pub(crate) fn write_i64(writer: &mut impl Write, num: i64) -> Result<(), Error> {
    writer.write(&num.to_le_bytes())?;
    Ok(())
}

pub(crate) fn write_f64(writer: &mut impl Write, num: f64) -> Result<(), Error> {
    writer.write(&num.to_le_bytes())?;
    Ok(())
}

use clap::{App, Arg, ArgMatches};
use image::{open, DynamicImage::*};

use stimmt::{Buffer, DataType};

fn main() {
    let matches = parse_args();

    let image = open(matches.value_of("INPUT_FILE").unwrap()).unwrap();

    match image {
        ImageLuma8(buf) => {
            let data = data_from_buffer(
                DataType::U8,
                buf.width(),
                buf.height(),
                1,
                buf.into_raw(),
            );
        }
        ImageRgb8(buf) => {
            let data = data_from_buffer(
                DataType::U8,
                buf.width(),
                buf.height(),
                3,
                buf.into_raw(),
            );
        }
        _ => {
            println!("Error: Unsupported image data format.");
        }
    }
}

fn parse_args() -> ArgMatches<'static> {
    App::new("sticonv")
        .version("0.1")
        .author("Francesco Pasa <francescopasa@gmail.com>")
        .about("Convert to and from the stimmt image format")
        .arg(
            Arg::with_name("INPUT_FILE")
                .help("Input file to use. The format is detected from the extension.")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT_FILE")
                .help(
                    "Output file to produce. The format is detected from the extension.",
                )
                .required(true)
                .index(2),
        )
        .get_matches()
}

fn data_from_buffer<T>(
    datatype: DataType,
    width: u32,
    height: u32,
    num_channels: u32,
    raw_data: Vec<T>,
) -> Buffer {
    match datatype {
        DataType::U8 => {
            let mut buf = Buffer::from_zeros(DataType::U8, width, height, num_channels);

            for y in 0..height {
                for x in 0..width {
                    let start = (y as usize * width as usize + x as usize)
                        * num_channels as usize;
                    let end = start + num_channels as usize;

                    buf.set_pixel_u8(x, y, &raw_data[start..end]);
                }
            }

            buf
        }
    }
}

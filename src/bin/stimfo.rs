use std::fmt::Display;
use std::path::Path;

use clap::{App, Arg, ArgMatches};

use stimmt::{StimmtImage, TagData};

fn main() {
    let matches = parse_args();
    let input_path = matches.value_of("INPUT_FILE").unwrap();

    let image = StimmtImage::open_path(input_path.as_ref()).unwrap();

    display_info(input_path.as_ref(), &image);
}

fn parse_args() -> ArgMatches<'static> {
    App::new("stimfo")
        .version("0.1")
        .author("Francesco Pasa <francescopasa@gmail.com>")
        .about("Display information about stimmt files.")
        .arg(
            Arg::with_name("INPUT_FILE")
                .help("stimmt file ro analyze.")
                .required(true)
                .index(1),
        )
        .get_matches()
}

fn display_info(path: &Path, image: &StimmtImage) {
    println!(
        "stimmt file: {}",
        std::fs::canonicalize(path).unwrap().display()
    );
    println!("data type: {}", image.datatype());
    println!("native size: {}, {}", image.width(), image.height());
    println!("channels: {}", image.num_channels());

    println!("tags:");
    for tag in image.tags() {
        print!("\t{}: ", tag.key);
        match tag.value {
            TagData::String(val) => println!("{}", val),
            TagData::Int(val) => println!("{}", val),
            TagData::IntArray(val) => println_vec(val),
            TagData::Float(val) => println!("{}", val),
            TagData::FloatArray(val) => println_vec(val),
        }
    }

    println!("overviews:");
    for scale_factor in image.overviews() {
        let w = image.width() / scale_factor;
        let h = image.height() / scale_factor;
        println!(
            "\tscale {}: {}, {} ({})",
            scale_factor,
            w,
            h,
            pixel_count(w as f64 * h as f64)
        );
    }
}

fn println_vec<T>(array: Vec<T>)
where
    T: Display,
{
    let len = array.len();
    if len == 0 {
        println!("[]");
        return;
    }

    print!("[");

    if len > 6 {
        array[..3].iter().for_each(|v| print!("{}, ", v));
        print!("[{} other values], ", len - 6);
        array[len - 4..len - 1]
            .iter()
            .for_each(|v| print!("{}, ", v));
    } else {
        array[..len - 1].iter().for_each(|v| print!("{}, ", v));
    }
    print!("{}", &array[len - 1]);

    println!("]");
}

fn pixel_count(mut count: f64) -> String {
    // order of magnitude
    let mut oom = 0;
    while count > 1000. {
        oom += 1;
        count /= 1000.;
    }

    match oom {
        0 => format!("{:.1} Pixels", count),
        1 => format!("{:.1} KPixels", count),
        2 => format!("{:.1} MPixels", count),
        3 => format!("{:.1} GPixels", count),
        4 => format!("{:.1} TPixels", count),
        _ => "".to_string(),
    }
}

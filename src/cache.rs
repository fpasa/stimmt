use super::*;

use std::fs;

pub(crate) trait Cache {
    fn get_cached_tile(&mut self, tile: &Tile) -> Result<Buffer, Error>;
    fn cache_tile(&mut self, tile: &mut Tile, data: &Buffer) -> Result<(), Error>;
    fn flush(&mut self) -> Result<(), Error>;
    fn close(&mut self) -> Result<(), Error>;
}

#[derive(Debug)]
pub(crate) struct FileCache {
    path: PathBuf,
    offset: u64,
    datatype: DataType,
    num_channels: u32,
    reader: BufReader<File>,
    writer: BufWriter<File>,
}

impl FileCache {
    pub(crate) fn new(datatype: DataType, num_channels: u32) -> Result<Self, Error> {
        let path = temp_file("stimmt_cache", "cache.stimmt");
        let file_w = File::create(&path)?;
        let file_r = File::open(&path)?;

        Ok(Self {
            path,
            offset: 0,
            datatype: datatype.clone(),
            num_channels,
            reader: BufReader::new(file_r),
            writer: BufWriter::new(file_w),
        })
    }
}

impl Cache for FileCache {
    fn get_cached_tile(&mut self, tile: &Tile) -> Result<Buffer, Error> {
        if tile.size == 0 {
            // tile does not exist yet
            Ok(Buffer::from_zeros(
                self.datatype,
                tile.width,
                tile.height,
                self.num_channels,
            ))
        } else {
            tile.read(&mut self.reader, self.datatype, self.num_channels)
        }
    }

    fn cache_tile(&mut self, tile: &mut Tile, data: &Buffer) -> Result<(), Error> {
        // just append new tiles at the end of the cache file, these will be rewritten in
        // the correct order afterwards anyways.
        tile.offset = self.offset;
        let size = tile.write(&mut self.writer, data)?;
        self.offset += size;
        Ok(())
    }

    fn flush(&mut self) -> Result<(), Error> {
        self.writer.flush()?;
        Ok(())
    }

    fn close(&mut self) -> Result<(), Error> {
        fs::remove_file(&self.path)?;
        Ok(())
    }
}

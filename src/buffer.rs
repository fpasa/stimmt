use super::*;
use std::any::Any;

#[derive(Debug, Eq, PartialEq)]
pub enum Buffer {
    U8Buffer(DataBuffer<u8>),
}

impl Buffer {
    pub fn from_zeros(
        datatype: DataType,
        width: u32,
        height: u32,
        num_channels: u32,
    ) -> Self {
        match datatype {
            DataType::U8 => {
                U8Buffer(DataBuffer::<u8>::from_zeros(width, height, num_channels))
            }
        }
    }

    pub fn from_value_u8(
        width: u32,
        height: u32,
        num_channels: u32,
        value: &[u8],
    ) -> Result<Self, Error> {
        Ok(U8Buffer(DataBuffer::from_value(
            width,
            height,
            num_channels,
            value,
        )?))
    }

    pub fn from_data_u8(
        width: u32,
        height: u32,
        num_channels: u32,
        data: &[u8],
    ) -> Result<Self, Error> {
        Ok(U8Buffer(DataBuffer::<u8>::from_data(
            width,
            height,
            num_channels,
            data,
        )?))
    }

    pub fn len(&self) -> u64 {
        match self {
            U8Buffer(data) => data.len() as u64,
        }
    }

    pub fn width(&self) -> u32 {
        match self {
            U8Buffer(data) => data.width,
        }
    }

    pub fn height(&self) -> u32 {
        match self {
            U8Buffer(data) => data.height,
        }
    }

    pub fn num_channels(&self) -> u32 {
        match self {
            U8Buffer(data) => data.num_channels,
        }
    }

    pub fn get_pixel(&mut self, x: u32, y: u32) -> Result<&[u8], Error> {
        match self {
            U8Buffer(data) => data.get_pixel(x, y),
        }
    }

    pub fn set_pixel_u8(&mut self, x: u32, y: u32, pixel: &[u8]) -> Result<(), Error> {
        match self {
            U8Buffer(data) => data.set_pixel(x, y, pixel),
        }
    }

    pub fn get_region(&self, bounds: &Bounds) -> Result<Buffer, Error> {
        match self {
            U8Buffer(data) => Ok(U8Buffer(data.get_region(bounds)?)),
        }
    }

    pub fn set_region(&mut self, bounds: &Bounds, other: &Buffer) -> Result<(), Error> {
        let err = Err(ValueError(
            "Can only set regions from buffers of the same variant".to_string(),
        ));

        match self {
            U8Buffer(data) => match other {
                U8Buffer(other_data) => data.set_region(bounds, other_data),
                _ => err,
            },
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DataBuffer<T> {
    width: u32,
    height: u32,
    num_channels: u32,
    data: Vec<T>,
}

impl<T> DataBuffer<T>
where
    T: Copy + Default,
{
    pub fn from_zeros(width: u32, height: u32, num_channels: u32) -> Self {
        let size = width as usize * height as usize * num_channels as usize;
        let data = vec![T::default(); size];
        DataBuffer {
            width,
            height,
            num_channels,
            data,
        }
    }

    pub fn from_value(
        width: u32,
        height: u32,
        num_channels: u32,
        value: &[T],
    ) -> Result<Self, Error> {
        if value.len() != num_channels as usize {
            return Err(ValueError(format!(
                "Trying to write value of length {} into buffer with {} channels.",
                value.len(),
                num_channels
            )));
        }

        let data = value.repeat(width as usize * height as usize);
        Ok(DataBuffer {
            width,
            height,
            num_channels,
            data,
        })
    }

    pub fn from_data(
        width: u32,
        height: u32,
        num_channels: u32,
        data: &[T],
    ) -> Result<Self, Error> {
        let size = width as usize * height as usize * num_channels as usize;

        if size != data.len() {
            return Err(ValueError(format!(
                "Trying to write data of length {} \
                into buffer of size {}, {} with {} channels, \
                which has total size of {}.",
                data.len(),
                width,
                height,
                num_channels,
                size
            )));
        }

        Ok(DataBuffer {
            width,
            height,
            num_channels,
            data: data.to_vec(),
        })
    }

    pub fn len(&self) -> u64 {
        self.data.len() as u64
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn num_channels(&self) -> u32 {
        self.num_channels
    }

    pub fn values(&self) -> &Vec<T> {
        &self.data
    }

    pub fn get_pixel(&mut self, x: u32, y: u32) -> Result<&[T], Error> {
        if x > self.width() || y > self.height() {
            return Err(ValueError(format!(
                "Trying to read pixel {}, {} outside buffer of size {}, {}.",
                x,
                y,
                self.width(),
                self.height()
            )));
        }

        let start =
            (y as usize * self.width as usize + x as usize) * self.num_channels as usize;
        let end =
            (y as usize * self.width as usize + x as usize) * self.num_channels as usize;
        Ok(&self.data[start..end])
    }

    pub fn set_pixel(&mut self, x: u32, y: u32, pixel: &[T]) -> Result<(), Error> {
        if x > self.width() || y > self.height() {
            return Err(ValueError(format!(
                "Trying to write pixel {}, {} outside buffer of size {}, {}.",
                x,
                y,
                self.width(),
                self.height()
            )));
        }

        if pixel.len() != self.num_channels as usize {
            return Err(ValueError(format!(
                "Trying to set pixel with {} values on buffer with {} channels.",
                pixel.len(),
                self.num_channels
            )));
        }

        let start =
            (y as usize * self.width as usize + x as usize) * self.num_channels as usize;
        let end =
            (y as usize * self.width as usize + x as usize) * self.num_channels as usize;
        self.data[start..end].copy_from_slice(pixel);

        Ok(())
    }

    // fn check_dim(&self, bounds: &Bounds, dim: &BufferDim) -> Result<(), Error> {
    //     if dim.num_channels != self.num_channels {
    //         return Err(BoundsError(format!(
    //             "Data has {} channels but image has {}",
    //             dim.num_channels, self.num_channels
    //         )));
    //     }
    //
    //     if bounds.w != dim.width || bounds.h != dim.height {
    //         return Err(BoundsError(format!(
    //             "Buffer of size {}, {} does not match bounds of size {}, {}",
    //             dim.width, dim.height, bounds.w, bounds.h,
    //         )));
    //     }
    //
    //     Ok(())
    // }

    pub fn get_region(&self, bounds: &Bounds) -> Result<DataBuffer<T>, Error> {
        if bounds.end_x() > self.width() || bounds.end_y() > self.height() {
            return Err(BoundsError(format!(
                "Trying to read region {} outside the buffer of size {}, {}.",
                bounds,
                self.width(),
                self.height()
            )));
        }

        let num_channels = self.num_channels() as usize;
        let size = bounds.w as usize * bounds.h as usize * num_channels;

        let mut region_data = Vec::with_capacity(size);

        for y in (bounds.y as usize)..(bounds.end_y() as usize) {
            let start = (y * self.width as usize + bounds.x as usize) * num_channels;
            let end = (y * self.width as usize + bounds.end_x() as usize) * num_channels;
            region_data.extend_from_slice(&self.data[start..end]);
        }

        Ok(DataBuffer {
            width: bounds.w,
            height: bounds.h,
            num_channels: self.num_channels,
            data: region_data,
        })
    }

    pub fn set_region(
        &mut self,
        bounds: &Bounds,
        other: &DataBuffer<T>,
    ) -> Result<(), Error> {
        if bounds.end_x() > self.width() || bounds.end_y() > self.height() {
            return Err(BoundsError(format!(
                "Trying to write region {} outside the buffer of size {}, {}.",
                bounds,
                self.width(),
                self.height()
            )));
        }
        // TODO: check dimensions & buffer type (move from check_dim())

        let num_channels = self.num_channels() as usize;

        for origin_y in 0..bounds.h as usize {
            let origin_start = origin_y * other.width as usize * num_channels;
            let origin_end = origin_start + bounds.w as usize * num_channels;

            let target_start = ((bounds.y as usize + origin_y as usize)
                * self.width as usize
                + bounds.x as usize)
                * num_channels;
            let target_end = target_start + bounds.w as usize * num_channels;

            self.data[target_start..target_end]
                .copy_from_slice(&other.data[origin_start..origin_end]);
        }

        Ok(())
    }
}

# stimmt format specification

## Data Types

stimmt files contain the following data types and formats:

- `strings` are utf-8 encoded sequences of bytes.
- `key` is the same as `string` but is always 4 bytes in length, used for field ids.
- `u32` are 32-bit unsigned integers used for most of the field values (apart from offsets).
- `u64` are 64-bit unsigned integers used for all offsets (to avoid the 4 GB file size limit).
- `u8` are 8-bit unsigned integers used for storing pixel data.

Exclusively for metadata, stimmt uses also

- `i64` are 64-bit signed integers.
- `f64` are 64-bit signed integers.

## Nomenclature

**Overviews** are set of tiles, representing the whole image at a specific resolution.
There is always an overview with scale factor 1 with the image data at native resolution,
and there might be multiple overviews with downscaled versions of the image data. 
For instance, a 20000x20000 pixel image might have an overview with scale factor 8 of size
2500x2500, containing the downscaled version of the image.

**Tags** are free fields (similar to TIFF tags or EXIF data) where data related to
the image can be stored. They can contain either `i64`, `f64` or `string` data or
arrays of `i64` and `f64` data. These fields are not meant to store large quantities
of data, but this is not enforced (the maximum field size is about 4 GB).

**Offsets** represent the offset in bytes from the beginning of the file to a specific
location. Offsets are always `u64` to allow for files larger than 4 GB.

## Endianness

All values in a stimmt files must be in little endian format.

## File header

The file header contains, in order, the following fields:

Field | Type | Description
--- | --- | ---
fingerprint | `key` | Always contains `"stim"`.
datatype | `u32` | Always set to zero (will be extended in the future).
width | `u32` | Width of the image.
height | `u32` | Height of the image.
num_channels | `u32` | Number of channels of the image.
num_overviews | `u32` | Number of overviews contained in the image. Must be `>= 1` (empty images are not allowed).
num_tags | `u32` | Number of metadata fields of the image.

The file header is always 28 bytes long.

## Overview header

The file header is followed directly by the overview header. The overview header
contains the following fields for each overview:

Field | Type | Description
--- | --- | ---
scale_factor | `u32` | The scale factor of the overview. There must always be a overview with scale factor 1. A scale factor of 2 means that the overview has *half* the resolution of the native image.
offset | `u64` | Offset to the beginning of the overview data.

These overview headers follow each other without any space between them.
The size of this header is `12*num_overviews` bytes.

Implementations should allow for any ordering of the overviews, but should
always write files with the overviews sorted in decreasing `scale_factor` order, 
to allow for fast visualization (removing the need to seek the whole file).

Scale factors should be power of two.

Scale factors should be in a reasonable range. It is recommended that overviews
with the shortest edge smaller than 64 pixel are not saved into images.
Implementations can ignore overviews smaller than this size, as well as degenerate
overviews which are smaller than one pixel.

## Tags

Directly following the overview header there is a list of tags, with corresponding value.
There are different types of tags, but all of them start with the following fields:

Field | Type | Description
--- | --- | ---
tag_key | `key` | A four-byte string containing the field identifier. A list of standard fields follows below. Custom fields must start with a `~`.
datatype | `u32` | A code for the data type of the tag. 

This tag header is followed by other fields and the data, according to the `datatype` of the tag

The following table lists the possible values for the `datatype` field.

Datatype | Description
--- | --- 
0 | String field
1 | `i64` field
2 | `i64` array
3 | `f64` field
4 | `f64` array

Below the detail of each field type.

Tags can be stored in any order, but implementations should write the standard files
at the beginning and custom fields afterward. Also, it's recommended that tags
are stored in alphabetical order.

### String (`datatype = 0`)

Contains string data (as for the rest of the format, it must be utf-8 encoded).
After the tag header above, the following field are found:

Field | Type | Description
--- | --- | ---
length | `u32` | Length of the string in bytes.
data | `u8 array` | String data.

For efficiency on certain architectures, the length of the string should always be padded with
zeros to a multiple of 4.

### Int and Float fields (`datatype = 1, 3`)

The tag header is followed by a single `i64` or `f64` value.

### Arrays  (`datatype = 2, 4`)

Contains array of values numeric values.

Field | Type | Description
--- | --- | ---
length | `u32` | Length of the array in number of elements.
data | `i64 of f64 array` | Array data.

### Standard tags

COMING SOON

## Overviews



COMING SOON

The baseline down-sampling method consist in just sampling one pixel every `sampling_factor`